# Scripts repository

This is a collection of codes developed by me. Each code have its own purpose.

Download the entire repository by typing:
```bash
git clone https://gitlab.com/rpcarvalho/codes
```
or just clone the desired script folder.

----
If you need, please contact rodrigo.carvalho@physics.uu.se.
