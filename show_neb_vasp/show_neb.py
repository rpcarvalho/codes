import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import interpolate
from os import popen as pop
'''
Created by Rodrigo de Carvalho
email: rodrigo.carvalho@physics.uu.se
'''
print("Created by Rodrigo de Carvalho")
print("email: rodrigo.carvalho@physics.uu.se")
folders=int(pop('find . -type d -name "0*" -o -name "1*" | wc -l').read())
energ=[]
step=[]
for n in range(folders):
    step.append(n)
    energ.append(float(pop("if [ -s "+str(n).zfill(2)+"/OUTCAR ] ; then grep TOTEN "+str(n).zfill(2)+"/OUTCAR | tail -n 1 | awk '{print $5}' ; \
            else echo '0' ; fi ").read())) 
df=pd.DataFrame(data={'step':step,'energ':energ})
df=df[df['energ'] != 0]
tck=interpolate.splrep(df.iloc[:,0],df.iloc[:,1],k=3)
a=df['step'].head(1).iloc[0]
b=df['step'].tail(1).iloc[0]
h=0.001
step_new=np.arange(a,b+2*h,h)
energ_new=interpolate.splev(step_new,tck,der=0)
my=energ_new.max()
mx=step_new[energ_new.argmax(0)]
data={'step':step_new,'energy':energ_new}
df2=pd.DataFrame(data)
df.to_csv('path.dat',sep=" ",header=True,index=False)
df2.to_csv('path_interp.dat',sep=" ",header=True,index=False)
fig, axs=plt.subplots(1,1,dpi=200)
plt.style.use('default')
axs.set_xticks(step)
axs.plot(step_new,energ_new,color='xkcd:azure', linewidth=1.3, aa=True,alpha=1.0,label='_nolegend_')
axs.plot(df.iloc[:,0],df.iloc[:,1],'o',color='xkcd:tomato', markersize=6, aa=True,alpha=0.9,label='_nolegend_')
axs.plot(mx,my,'o',color='xkcd:violet', markersize=6, aa=True,alpha=0.7,label='Saddle point: '+str(format(my,'0.2f'))+' eV')
axs.set_ylabel('Energy (eV)',fontsize=11)
axs.set_xlabel(r'Reaction path',fontsize=11)
axs.legend(fontsize=9,loc=1,shadow=False,fancybox=True)
plt.tight_layout()
fig.savefig('nebplot.png')
fig.savefig('nebplot.pdf')
plt.show()