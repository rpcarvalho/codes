## Tutorial
----

Just put the show_neb.py in the same folder of VASP calculations (folders 00, 01, 02, etc)
If you have results for initial and final images just put it in the respective folders and
the script will plot it as well.

You can run the script by typing:
```bash
python show_neb.py
```

![Example](nebplot.png)

It is possible to use path.dat and path_interp.dat to further analysis/plots.

### Dependencies
* numpy
* scipy
* pandas
* matplotlib
* python3

----
Developed and maintained by Rodrigo de Carvalho
rodrigo.carvalho@physics.uu.se
